# answer end question

# Bagian list

# 1.	Buat sebuah list buah-buahan yang berisi 6 jenis buah . Ketika program di jalankan hanya menampilkan urutan buat ke 4 dan ke 5 secara berurutan. [10 point] pengerjaan 3 menit
# Jawaban
thislist = ["apple", "banana", "cherry", "melon", "pisang", "pepaya"]
print(thislist[3:5])
print(thislist[-3:-1])

# 2.	Change the value from "apple" to "kiwi", in the fruits list. [10 point]
# pengerjaan 3 menit
fruits = ["apple", "banana", "cherry"]
# jawaban
fruits[0] = "apple"
print(fruits)

# 3.	Use the append method to add "orange" to the fruits list. [10 point]
# pengerjaan 3 menit
fruits = ["apple", "banana", "cherry"]
# jawaban
fruits.append("orange")

# 4.	Use the insert method to add "lemon" as the second item in the fruits list. [10 point]
# pengerjaan 3 menit
fruits = ["apple", "banana", "cherry"]
# jawaban
fruits.insert(1, "lemon")

# 5.	Use the remove method to remove "banana" from the fruits list. [10 point]
# pengerjaan 3 menit
fruits = ["apple", "banana", "cherry"]
# jawaban
fruits.remove("banana")

# 6.	Use negative indexing to print the last item in the list. [10 point] pengerjaan 3 menit
fruits = ["apple", "banana", "cherry"]
# jawaban
print(fruits[-1])

# 7.	Use a range of indexes to print the third, fourth, and fifth item in the list. [10 point]
# pengerjaan 3 menit
fruits = ["apple", "banana", "cherry", "orange", "kiwi", "melon", "mango"]
# jawaban
print(fruits[2:4])

# 8.	Buatlah sebuah program yang menghapus semua banana dalam list. List akan melakukan pergantian otomatis dengan menggunakan fitur remove dan code perulangan. [30 point] pengerjaan 9 menit
hewan = ["cat", "banana", "dog", "zebra", "banana", "lion", "banana", ]
# output program
['cat', 'dog', 'zebra', 'lion']
# jawaban
for x in hewan:
    if x == "banana":
        hewan.remove("banana")
print(hewan)


# Bagian String

# 1.	 Terdapat kumpulan variable dengan data string sebagai berikut (pengerjaan 3 menit)
# Rangkaikan kata I love python dari kalimat pada sting berikut
kata = "I Love Kamu Python"
"Output"
"Kata: I Love Pyhton"
# Jawaban
print("kata:", kata[0:6], kata[12:])


# Bagian Tuple

# 1.	Use the correct syntax to print the first item in the fruits tuple (pengerjaan 3 menit)
fruits = ("apple", "banana", "cherry")
# jawaban
print(fruits[0])
# 2.	Use the correct syntax to print the number of items in the fruits tuple. (pengerjaan 3 menit)
fruits = ("apple", "banana", "cherry")
# jawaban
print(len(fruits))
# 3.	Use negative indexing to print the last item in the tuple. (pengerjaan 3 menit)
fruits = ("apple", "banana", "cherry")
#  jawaban
print(fruits[-1])

# 4.	Write a program to search the word “appel” in a turtle (pengerjaan 4 menit)
thistuple = ("apple", "banana", "cherry")
# output program
# Yes, 'apple' is in the fruits tuple
# jawaban
if "apple" in thistuple:
    print("Yes, 'apple' is in the fruits tuple")

# Bagian Dictionaries

# 1.	Use the get method to print the value of the "model" key of the car dictionary. (pengerjaan 3 menit)
car = {
    "brand": "Ford",
    "model": "Mustang",
    "year": 1964
}
# Jawaban
print(car.get("model"))
# 2.	Change the "year" value from 1964 to 2020. (pengerjaan 3 menit)
# Jawaban
car["year"] = 2020
# 3.	Add the key/value pair "color" : "red" to the car dictionary. (pengerjaan 3 menit)
# Jawaban
car["color"] = "red"

# 4.	Tampilkan semua key dan value dalam sebuat dictionary python berikut (pengerjaan 3 menit)
thisdict = {
    "brand": "Ford",
    "model": "Mustang",
    "year": 1964
}
# Output:
# brand Ford
# model Mustang
# year 1964

# jawaban
for x, y in thisdict.items():
    print(x, y)
