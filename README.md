# pemtek List-tuple-string-Dictionaries

## clone this answer

1. install git bash
   https://git-scm.com/downloads
2. open your git bash and write
   git clone https://gitlab.com/rahmansubrata/pemtek-list-tuple-string-dictionaries.git
3. finish

## Tuple

Tuples are used to store multiple items in a single variable.
Tuple is one of 4 built-in data types in Python used to store collections of data, the other 3 are List, Set, and Dictionary, all with different qualities and usage.
A tuple is a collection which is ordered and unchangeable.
Tuples are written with round brackets.

## List

Lists are used to store multiple items in a single variable.
Lists are one of 4 built-in data types in Python used to store collections of data, the other 3 are Tuple, Set, and Dictionary, all with different qualities and usage.
Lists are created using square brackets:

## Set

Sets are used to store multiple items in a single variable.
Set is one of 4 built-in data types in Python used to store collections of data, the other 3 are List, Tuple, and Dictionary, all with different qualities and usage.
A set is a collection which is both unordered and unindexed.

## Dictionary

Dictionaries are used to store data values in key:value pairs.
A dictionary is a collection which is ordered\*, changeable and does not allow duplicates.
As of Python version 3.7, dictionaries are ordered. In Python 3.6 and earlier, dictionaries are unordered.
Dictionaries are written with curly brackets, and have keys and values:

Sets are written with curly brackets.

## Description

code answer engineering programing

## Installation

enjoy to run your program

## Authors and acknowledgment

follow me at

ig : https://www.instagram.com/rahmansubrata/?hl=en

email : rahmansubrata00@gmail.com

Show your appreciation to those who have contributed to the project.
https://www.w3schools.com/

## sponsor

http://sahabattutor.com
join to teach and to learning with sahabat tutor.
visit our social media
ig : https://www.instagram.com/sahabattutor/?hl=en
